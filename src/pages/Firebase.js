import firebase from 'firebase/app'

const firebaseConfig = {
    apiKey: "AIzaSyBB2grC2l92mGSnFnId5yUPD-3oQdSXg8c",
    authDomain: "myreact-66cbf.firebaseapp.com",
    databaseURL: "https://myreact-66cbf-default-rtdb.firebaseio.com",
    projectId: "myreact-66cbf",
    storageBucket: "myreact-66cbf.appspot.com",
    messagingSenderId: "692397834232",
    appId: "1:692397834232:web:168b21a3c15c6e58aba00d",
    measurementId: "G-Q2Q91993NX"


};
firebase.initializeApp(firebaseConfig);

export default firebase;