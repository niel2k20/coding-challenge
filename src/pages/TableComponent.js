
const TableComponent = (props) => {
    function renderAppName() {

        let name = [];
        props.users.map((x, i) => {
            props.accounts.map((y, i) => {

                if (x.account === y.id) {
                    name.push(
                        <tr key={y.id}>
                            <td>{renderName(y.id)}</td>
                            <td>{y.appName}</td>
                            <td>{y.title[0].title}</td>

                        </tr>
                    )
                }
            })
        });
        return name;

    }
    function renderName(id) {

        let appname = [];
        props.users.map((x, i) => {

            if (x.account === id) {
                appname.push(
                    x.name
                )
            }
            return appname;
        });
        return appname;

    }



    return (
        <table className="ui celled table">
            <thead>
                <tr><th>Name</th>
                    <th>Account Name</th>
                    <th>Title</th>
                </tr></thead>
            <tbody>

                {renderAppName()}


            </tbody>
        </table>
    );
}

export default TableComponent;