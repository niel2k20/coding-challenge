import React, { Component } from 'react'
import ReactStars from "react-rating-stars-component";
import firebase from './Firebase';
import 'firebase/firestore';


export default class StarratingComponent extends Component {
    constructor() {
        super();

        this.state = {
            rating: 4,
            message: ''
        };
    }
    ratingChanged(nextValue, prevValue, name) {
        this.setState({ rating: nextValue });
    }
    onTextChange = (value) => {

        console.log(value);
    }
    onFormSubmit = (e) => {
        e.preventDefault();
        console.log('i m here');
        this.setState({
            rating: 4,
            message: ''
        })
        firebase.firestore().collection("contacts")
            .add({
                rating: this.state.rating,
                message: this.state.message
            })
            .then(() => {

                this.setState({
                    rating: 4,
                    message: ''
                });
                alert("Your message has been submitted👍");
            })
            .catch((error) => {
                alert(error.message);
            });


    }

    render() {
        return (
            <div className="ui two column centered grid">
                <div className="column">
                    <form onSubmit={(e) => this.onFormSubmit(e)}>
                        <label>Rating</label>
                        <ReactStars
                            count={5}
                            onChange={(e) => this.setState({
                                rating: e
                            })}
                            value={this.state.rating}
                            size={24}
                            activeColor="#ffd700"
                        />
                        <div className="ui form">
                            <div className="field">
                                <label>Comments</label>
                                <textarea rows={2}
                                    value={this.state.message}
                                    onChange={(e) => this.setState({ message: e.target.value })}
                                />
                            </div>
                        </div><br />
                        <button className="ui primary button">
                            Submit
                </button>
                    </form>
                </div>
            </div>


        );
    };
}
