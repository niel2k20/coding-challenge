import React, { Component } from 'react'
import './App.css';

import firebase from './pages/Firebase';
import 'firebase/database'
import TableComponent from './pages/TableComponent';
import StarratingComponent from './pages/StarratingComponent';


export default class App extends Component {

  constructor(props) {
    super(props);
    this.state = {
      accounts: [],
      users: [],
    }
  }

  componentDidMount() {

    const accountRef = firebase.database().ref('accounts');
    accountRef.on('value', (snapshot) => {
      let newAccState = [];

      snapshot.forEach(data => {
        const dataVal = data.val()
        newAccState.push({
          id: data.key,
          appName: Object.keys(dataVal.apps),
          title: Object.values(dataVal.apps),
          data: dataVal.apps,
        });

      });
      this.setState({
        accounts: newAccState
      })
    })
    const userRef = firebase.database().ref('users');
    userRef.on('value', (snapshot) => {
      let newUsersState = [];
      snapshot.forEach(data => {
        const dataVal = data.val()
        newUsersState.push({
          id: data.key,
          name: dataVal.name,
          account: dataVal.account
        });
      });
      this.setState({
        users: newUsersState
      })

    })
  }

  render() {

    return <div>
      <TableComponent accounts={this.state.accounts} users={this.state.users} />
      <StarratingComponent />
    </div>

  };
}
